const plantData = require('./plantData.json')
const { PlantSynth, stopAll } = require('./plant.js')

let context = null
let sink = null
let plants = []

const playButton = document.getElementById('play')
let playing = false

playButton.addEventListener("click", () => {
  context = context || new AudioContext()
  if(!sink) {
    const compressor = context.createDynamicsCompressor()
    compressor.threshold.setValueAtTime(-50, context.currentTime)
    compressor.knee.setValueAtTime(40, context.currentTime)
    compressor.ratio.setValueAtTime(12, context.currentTime)
    compressor.attack.setValueAtTime(0, context.currentTime)
    compressor.release.setValueAtTime(0.25, context.currentTime)
    compressor.connect(context.destination)
    sink = compressor
  }

  if(playing) {
    stopAll(Array.from(plants.values()))
    playButton.innerHTML = 'Play'
    playing = false
  } else {
    plants = new Map(
      plantData
        .filter(({Quadrat}) => Quadrat === "97.00")
        .map(({Latin, Frequency, Amplitude, Panning, Wave}) =>
          [Latin, new PlantSynth(Frequency, Amplitude, Panning, Wave, {
            context, sink
          })]
        ))
    playButton.innerHTML = 'Stop'
    playing = true
  }
})

// stopAll([plant1, plant2, plant3, plant4, plant5, plant6, plant7, plant8])
