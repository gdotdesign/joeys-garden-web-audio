const plantData = require('./data/plantData.json')

function audioChain (...nodes) {
  nodes.reduce((current, next) => {
    current.connect(next)
    return next
  })
}

class PlantSynth {
  constructor(freq, amp, pan, timbre = 'sine',
              {
                context,
                sink = context.destination
              }) {
    this.params = {
      freq, amp, pan, timbre
    }
    this.context = context
    this.sink = sink
    this.start()
  }

  start() {
    const {context, sink, params: {freq, amp, pan, timbre}} = this

    const node = context.createOscillator()
    node.type = timbre
    node.frequency.value = freq
    const gain = context.createGain()
    gain.gain.setValueAtTime(0.01, context.currentTime)
    gain.gain.setTargetAtTime(amp, context.currentTime, 1)
    gain.gain.linearRampToValueAtTime(amp, context.currentTime + 1)

    const panner = context.createStereoPanner()
    panner.pan.value = pan

    audioChain(node, gain, panner, sink)
    node.start(0)

    this.gain = gain
    this.playing = true
    this.node = node
    return node
  }

  stop(decayTime = 1.5) {
    // decayTime in seconds
    const noteEndTime = this.context.currentTime + decayTime
    this.playing = false
    this.node.stop(noteEndTime)

    if (this.gain.gain.cancelAndHoldAtTime) {
      this.gain.gain.cancelAndHoldAtTime(this.context.currentTime)
    } else {
      this.gain.gain.cancelScheduledValues(this.context.currentTime);
    }

    this.gain.gain.setTargetAtTime(0.001, this.context.currentTime, decayTime)
    this.gain.gain.exponentialRampToValueAtTime(0.001, noteEndTime)
  }
}

function stopAll (nodes) {
  nodes.forEach(node => node.stop())
}

window.PlantSynth = PlantSynth
window.stopAll = stopAll
window.plantData = plantData
