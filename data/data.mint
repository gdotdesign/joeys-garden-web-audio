[
[
{
amplitude = 0.10,
common = "Florida peperomia, Baby rubberplant",
frequency = 234.82,
latin = "Peperomia obtusifolia",
ordinal = 1,
panning = 1,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Adam's needle",
frequency = 249.42,
latin = "Yucca filamentosa",
ordinal = 2,
panning = -0.94,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Smallcane, Florida tibisee, Wild-bamboo",
frequency = 314.81,
latin = "Lasiacis divaricata",
ordinal = 3,
panning = 0.88,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Woodsgrass, Basketgrass",
frequency = 319.64,
latin = "Oplismenus hirtellus subsp. setarius",
ordinal = 4,
panning = -0.82,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Grape (cultivar – Concord) ",
frequency = 340.42,
latin = "Vitis labrusca",
ordinal = 5,
panning = 0.76,
quadrat = 1,
wave = "square"
},
{
amplitude = 0.10,
common = "Longstalked-stopper",
frequency = 363.74,
latin = "Mosiera longipes",
ordinal = 6,
panning = -0.70,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Wild-lime, Lime prickly-ash",
frequency = 419.29,
latin = "Zanthoxylum fagara",
ordinal = 7,
panning = 0.64,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Coralbean, Cherokee bean",
frequency = 547.70,
latin = "Erythrina herbacea",
ordinal = 8,
panning = -0.58,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "White mulberry",
frequency = 609.48,
latin = "Morus alba",
ordinal = 9,
panning = 0.52,
quadrat = 1,
wave = "triangle"
},
{
amplitude = 0.10,
common = "Virginia live oak",
frequency = 636.80,
latin = "Quercus virginiana",
ordinal = 10,
panning = -0.48,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Rhacoma, Maidenberry",
frequency = 720.61,
latin = "Crossopetalum rhacoma",
ordinal = 11,
panning = 0.42,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Corkystem passionflower",
frequency = 809.26,
latin = "Passiflora suberosa",
ordinal = 12,
panning = -0.36,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Hog-plum, Tallowwood",
frequency = 868.92,
latin = "Ximenia americana",
ordinal = 13,
panning = 0.30,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Rougeplant, Bloodberry",
frequency = 889.99,
latin = "Rivina humilis",
ordinal = 14,
panning = -0.24,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scorpionstail",
frequency = 1787.40,
latin = "Heliotropium angiospermum",
ordinal = 15,
panning = 0.18,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Common snowberry, Milkberry",
frequency = 1835.38,
latin = "Chiococca alba",
ordinal = 16,
panning = -0.12,
quadrat = 1,
wave = "sine"
},
{
amplitude = 0.10,
common = "Coral honeysuckle, Trumpet honeysuckle",
frequency = 4276,
latin = "Lonicera sempervirens",
ordinal = 17,
panning = 0.06,
quadrat = 1,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Florida arrowroot, coontie",
frequency = 225.57,
latin = "Zamia integrifolia",
ordinal = 1,
panning = -1,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Cinnamon bark, Pepper cinnamon, Wild cinnamon",
frequency = 232.41,
latin = "Canella winterana",
ordinal = 2,
panning = 0.92,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida butterfly orchid",
frequency = 242.50,
latin = "Encyclia tampensis",
ordinal = 3,
panning = -0.84,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Spanish-moss",
frequency = 292.65,
latin = "Tillandsia usneoides",
ordinal = 4,
panning = 0.76,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Variable witchgrass",
frequency = 310.12,
latin = "Dichanthelium commutatum",
ordinal = 5,
panning = -0.68,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Lignumvitae, Holywood lignumvitae",
frequency = 346,
latin = "Guaiacum sanctum",
ordinal = 6,
panning = 0.60,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Powderpuff, Sunshine mimosa",
frequency = 489.29,
latin = "Mimosa strigillosa",
ordinal = 7,
panning = -0.52,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Yellow necklacepod",
frequency = 504.31,
latin = "Sophora tomentosa var. truncata",
ordinal = 8,
panning = 0.44,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Quailberry, Christmasberry",
frequency = 712.43,
latin = "Crossopetalum ilicifolium",
ordinal = 9,
panning = -0.36,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Corkystem passionflower",
frequency = 809.26,
latin = "Passiflora suberosa",
ordinal = 10,
panning = 0.28,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tough Florida bully",
frequency = 1030,
latin = "Sideroxylon tenax",
ordinal = 11,
panning = -0.20,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "West Indian pinkroot",
frequency = 1082.33,
latin = "Spigelia anthelmia",
ordinal = 12,
panning = 0.12,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Late purple aster, clasping aster",
frequency = 1181.50,
latin = "Symphyotrichum patens",
ordinal = 13,
panning = -0.04,
quadrat = 12,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pineland heliotrope",
frequency = 1763.93,
latin = "Heliotropium polyphyllum",
ordinal = 14,
panning = 0,
quadrat = 12,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Brittle maidenhair",
frequency = 213.74,
latin = "Adiantum tenerum",
ordinal = 1,
panning = -1,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Goldenrod fern",
frequency = 216.36,
latin = "Pityrogramma trifoliata",
ordinal = 2,
panning = 0.76,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Southern shield fern",
frequency = 221.33,
latin = "Thelypteris kunthii",
ordinal = 3,
panning = -0.74,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Cinnamon bark, Pepper cinnamon, Wild cinnamon",
frequency = 232.41,
latin = "Canella winterana",
ordinal = 4,
panning = 0.70,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bulltongue arrowhead, lance-leaved arrowhead",
frequency = 237.31,
latin = "Sagittaria lancifolia",
ordinal = 5,
panning = -0.66,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Narrowleaf blueeyed-grass",
frequency = 246.59,
latin = "Sisyrinchium angustifolium",
ordinal = 6,
panning = 0.64,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Andropogon sp",
frequency = 305.56,
latin = "Andropogon sp",
ordinal = 7,
panning = -0.60,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Variable witchgrass",
frequency = 310.12,
latin = "Dichanthelium commutatum",
ordinal = 8,
panning = 0.56,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Lignumvitae, Holywood lignumvitae",
frequency = 346,
latin = "Guaiacum sanctum",
ordinal = 9,
panning = -0.52,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Southern gaura, Southern beeblossum",
frequency = 383.08,
latin = "Oenothera simulans",
ordinal = 10,
panning = 0.48,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scarlet rosemallow",
frequency = 461.30,
latin = "Hibiscus coccineus",
ordinal = 11,
panning = -0.46,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Everglades key pencilflower",
frequency = 525.37,
latin = "Stylosanthes calcicola",
ordinal = 12,
panning = 0.42,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Artillery fern, Artillery plant, Rockweed",
frequency = 622.94,
latin = "Pilea microphylla",
ordinal = 13,
panning = -0.40,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pineland poinsettia, Pineland spurge",
frequency = 651.07,
latin = "Euphorbia pinetorum ",
ordinal = 14,
panning = 0.36,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Wild plumbago, Doctorbush, Florida plumbago, White plumbago, Devil’s plumbago",
frequency = 848.46,
latin = "Plumbago zeylanica",
ordinal = 15,
panning = -0.32,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Rougeplant, Bloodberry",
frequency = 889.99,
latin = "Rivina humilis",
ordinal = 16,
panning = 0.28,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "West Indian pinkroot",
frequency = 1082.33,
latin = "Spigelia anthelmia",
ordinal = 17,
panning = -0.24,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Firebush",
frequency = 1935.60,
latin = "Hamelia patens var. patens",
ordinal = 18,
panning = 0.20,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Water hyssop, Herb-of-grace",
frequency = 2749.21,
latin = "Bacopa monnieri",
ordinal = 19,
panning = -0.18,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Lemon hyssop, Lemon bacopa, Blue waterhyssop",
frequency = 2787.02,
latin = "Bacopa caroliniana",
ordinal = 20,
panning = 0.16,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "American beautyberry",
frequency = 2864.29,
latin = "Callicarpa americana",
ordinal = 21,
panning = -0.14,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Browne's savory",
frequency = 3241,
latin = "Clinopodium brownei",
ordinal = 22,
panning = 0.10,
quadrat = 16,
wave = "sine"
},
{
amplitude = 0.10,
common = "Water dropwort, Water cowbane",
frequency = 4396.90,
latin = "Tiedmannia filiformis",
ordinal = 23,
panning = -0.06,
quadrat = 16,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Whitemouth dayflower",
frequency = 277.09,
latin = "Commelina erecta",
ordinal = 1,
panning = 1,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pineapple",
frequency = 296.83,
latin = "Ananas comosus",
ordinal = 2,
panning = -0.98,
quadrat = 20,
wave = "square"
},
{
amplitude = 0.10,
common = "Schizachyrium sp",
frequency = 301.13,
latin = "Schizachyrium sp",
ordinal = 3,
panning = 0.94,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Variable witchgrass",
frequency = 310.12,
latin = "Dichanthelium commutatum",
ordinal = 4,
panning = -0.92,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Southern gaura, Southern beeblossum",
frequency = 383.08,
latin = "Oenothera simulans",
ordinal = 5,
panning = 0.90,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Common torchwood, Sea torchwood",
frequency = 431.21,
latin = "Amyris elemifera",
ordinal = 6,
panning = -0.86,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Poeppig’s rosemallow, Fairy hibiscus",
frequency = 456.67,
latin = "Hibiscus poeppigii ",
ordinal = 7,
panning = 0.80,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Powderpuff, Sunshine mimosa",
frequency = 489.29,
latin = "Mimosa strigillosa",
ordinal = 8,
panning = -0.74,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Sweet acacia",
frequency = 494.22,
latin = "Vachellia farnesiana var. farnesiana",
ordinal = 9,
panning = 0.68,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida prairieclover ",
frequency = 514.69,
latin = "Dalea floridana",
ordinal = 10,
panning = -0.62,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Darlingplum",
frequency = 559.35,
latin = "Reynosia septentrionalis",
ordinal = 11,
panning = 0.54,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Artillery fern, Artillery plant, Rockweed",
frequency = 622.94,
latin = "Pilea microphylla",
ordinal = 12,
panning = -0.48,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pineland poinsettia, Pineland spurge",
frequency = 651.07,
latin = "Euphorbia pinetorum ",
ordinal = 13,
panning = 0.42,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "West Indian pinkroot",
frequency = 1082.33,
latin = "Spigelia anthelmia",
ordinal = 14,
panning = -0.36,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Joewood",
frequency = 1109.65,
latin = "Jacquinia keyensis",
ordinal = 15,
panning = 0.30,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida elephant’s-foot, Tall elephant’s-foot",
frequency = 1152.13,
latin = "Elephantopus elatus",
ordinal = 16,
panning = -0.24,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pinebarren goldenrod",
frequency = 1196.50,
latin = "Solidago fistulosa",
ordinal = 17,
panning = 0.20,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Unknown goldenrod 1",
frequency = 1227.17,
latin = "Solidago sp1",
ordinal = 18,
panning = -0.16,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Spanish-needles",
frequency = 1307.82,
latin = "Bidens alba var. radiata",
ordinal = 19,
panning = 0.12,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Caribbean princewood",
frequency = 1987.92,
latin = "Exostema caribaeum",
ordinal = 20,
panning = -0.08,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Wild basil, Wild sweet basil",
frequency = 2984.46,
latin = "Ocimum campechianum",
ordinal = 21,
panning = 0.06,
quadrat = 20,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 22,
panning = -0.04,
quadrat = 20,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Southern shield fern",
frequency = 221.33,
latin = "Thelypteris kunthii",
ordinal = 1,
panning = 1,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "American white waterlily",
frequency = 230.06,
latin = "Nymphaea odorata",
ordinal = 2,
panning = -0.92,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bulltongue arrowhead, lance-leaved arrowhead",
frequency = 237.31,
latin = "Sagittaria lancifolia",
ordinal = 3,
panning = 0.84,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tape-grass, American eel-grass",
frequency = 239.87,
latin = "Vallisneria americana",
ordinal = 4,
panning = -0.76,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Rain-lily, Redmargin zephyrlily",
frequency = 261.60,
latin = "Zephyranthes simpsonii",
ordinal = 5,
panning = 0.68,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pickerelweed",
frequency = 284.65,
latin = "Pontederia cordata",
ordinal = 6,
panning = -0.60,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Variable witchgrass",
frequency = 310.12,
latin = "Dichanthelium commutatum",
ordinal = 7,
panning = 0.52,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "White-ironwood, Inkwood",
frequency = 411.63,
latin = "Hypelate trifoliata",
ordinal = 8,
panning = -0.44,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Coffee colubrina, Greenheart",
frequency = 571.34,
latin = "Colubrina arborescens",
ordinal = 9,
panning = 0.36,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Gopher-apple",
frequency = 772.25,
latin = "Licania michauxii ",
ordinal = 10,
panning = -0.32,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "West Indian pinkroot",
frequency = 1082.33,
latin = "Spigelia anthelmia",
ordinal = 11,
panning = 0.28,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Stokes’ aster",
frequency = 1137.77,
latin = "Stokesia laevis",
ordinal = 12,
panning = -0.24,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Blanketflower, Firewheel",
frequency = 1291.22,
latin = "Gaillardia pulchella",
ordinal = 13,
panning = 0.20,
quadrat = 30,
wave = "triangle"
},
{
amplitude = 0.10,
common = "Florida tickseed",
frequency = 1324.66,
latin = "Coreopsis floridana",
ordinal = 14,
panning = -0.16,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 15,
panning = 0.12,
quadrat = 30,
wave = "sine"
},
{
amplitude = 0.10,
common = "Wild-sage, Buttonsage",
frequency = 3988.59,
latin = "Lantana involucrata",
ordinal = 16,
panning = -0.08,
quadrat = 30,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Giant leather fern",
frequency = 211.74,
latin = "Acrostichum danaeifolium",
ordinal = 1,
panning = -1,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Adiantum sp",
frequency = 214.47,
latin = "Adiantum sp",
ordinal = 2,
panning = 0.90,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tailed bracken fern",
frequency = 218.30,
latin = "Pteridium pseudocaudatum",
ordinal = 3,
panning = -0.80,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Southern shield fern",
frequency = 221.33,
latin = "Thelypteris kunthii",
ordinal = 4,
panning = 0.70,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tape-grass, American eel-grass",
frequency = 239.87,
latin = "Vallisneria americana",
ordinal = 5,
panning = -0.62,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Swamp-lily, Seven-sisters, String-lily",
frequency = 258.42,
latin = "Crinum americanum",
ordinal = 6,
panning = 0.54,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Rain-lily, Redmargin zephyrlily",
frequency = 261.60,
latin = "Zephyranthes simpsonii",
ordinal = 7,
panning = -0.46,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Woodsgrass, Basketgrass",
frequency = 319.64,
latin = "Oplismenus hirtellus subsp. setarius",
ordinal = 8,
panning = 0.38,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Longstalked-stopper",
frequency = 363.74,
latin = "Mosiera longipes",
ordinal = 9,
panning = -0.34,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Coffee colubrina, Greenheart",
frequency = 571.34,
latin = "Colubrina arborescens",
ordinal = 10,
panning = 0.28,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Artillery fern, Artillery plant, Rockweed",
frequency = 622.94,
latin = "Pilea microphylla",
ordinal = 11,
panning = -0.22,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Wild dilly",
frequency = 1004.95,
latin = "Manilkara jaimiqui subsp. emarginata",
ordinal = 12,
panning = 0.18,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida tickseed",
frequency = 1324.66,
latin = "Coreopsis floridana",
ordinal = 13,
panning = -0.14,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Firebush",
frequency = 1935.60,
latin = "Hamelia patens var. patens",
ordinal = 14,
panning = 0.10,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 15,
panning = -0.06,
quadrat = 44,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida fiddlewood",
frequency = 3670,
latin = "Citharexylum spinosum",
ordinal = 16,
panning = 0.02,
quadrat = 44,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Florida arrowroot, coontie",
frequency = 225.57,
latin = "Zamia integrifolia",
ordinal = 1,
panning = -1,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Red cedar",
frequency = 227.78,
latin = "Juniperus virginiana",
ordinal = 2,
panning = 0.94,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Spanish-bayonet, Aloe yucca",
frequency = 250.87,
latin = "Yucca aloifolia",
ordinal = 3,
panning = -0.88,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Unknown thatch palm",
frequency = 273.47,
latin = "Thrinax sp ",
ordinal = 4,
panning = 0.82,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Biscayne prickly-ash",
frequency = 423.21,
latin = "Zanthoxylum coriaceum",
ordinal = 5,
panning = -0.76,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Sweet acacia",
frequency = 494.22,
latin = "Vachellia farnesiana var. farnesiana",
ordinal = 6,
panning = 0.70,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Yellow necklacepod",
frequency = 504.31,
latin = "Sophora tomentosa var. truncata",
ordinal = 7,
panning = -0.64,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida prairieclover ",
frequency = 514.69,
latin = "Dalea floridana",
ordinal = 8,
panning = 0.58,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Coffee colubrina, Greenheart",
frequency = 571.34,
latin = "Colubrina arborescens",
ordinal = 9,
panning = -0.52,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Maidenbush, Bahama maidenbush",
frequency = 696.42,
latin = "Heterosavia bahamensis",
ordinal = 10,
panning = 0.46,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Rougeplant, Bloodberry",
frequency = 889.99,
latin = "Rivina humilis",
ordinal = 11,
panning = -0.40,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "San pedro cactus",
frequency = 980.61,
latin = "Echinopsis pachanoi",
ordinal = 12,
panning = 0.34,
quadrat = 65,
wave = "square"
},
{
amplitude = 0.10,
common = "Seaside goldenrod",
frequency = 1211.72,
latin = "Solidago sempervirens",
ordinal = 13,
panning = -0.28,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scorpionstail",
frequency = 1787.40,
latin = "Heliotropium angiospermum",
ordinal = 14,
panning = 0.22,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Everglades velvetseed, Hammock velvetseed",
frequency = 1884.77,
latin = "Guettarda elliptica",
ordinal = 15,
panning = -0.16,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Wild-allamanda, Hammock viperstail",
frequency = 2213,
latin = "Pentalinon luteum",
ordinal = 16,
panning = 0.12,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 17,
panning = -0.08,
quadrat = 65,
wave = "sine"
},
{
amplitude = 0.10,
common = "Blue porterweed, Joee",
frequency = 3569.79,
latin = "Stachytarpheta jamaicensis",
ordinal = 18,
panning = 0.04,
quadrat = 65,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Swamp-lily, Seven-sisters, String-lily",
frequency = 258.42,
latin = "Crinum americanum",
ordinal = 1,
panning = 1,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tillandsia sp",
frequency = 288.59,
latin = "Tillandsia sp",
ordinal = 2,
panning = -0.96,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Variable witchgrass",
frequency = 310.12,
latin = "Dichanthelium commutatum",
ordinal = 3,
panning = 0.92,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Woodsgrass, Basketgrass",
frequency = 319.64,
latin = "Oplismenus hirtellus subsp. setarius",
ordinal = 4,
panning = -0.88,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Grape (cultivar – Concord) ",
frequency = 340.42,
latin = "Vitis labrusca",
ordinal = 5,
panning = 0.84,
quadrat = 66,
wave = "square"
},
{
amplitude = 0.10,
common = "Gumbo-limbo",
frequency = 404.18,
latin = "Bursera simaruba",
ordinal = 6,
panning = -0.80,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida prairieclover ",
frequency = 514.69,
latin = "Dalea floridana",
ordinal = 7,
panning = 0.76,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Nakedwood, Soldierwood",
frequency = 583.69,
latin = "Colubrina elliptica",
ordinal = 8,
panning = -0.72,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "American elm, Florida elm",
frequency = 596.40,
latin = "Ulmus americana",
ordinal = 9,
panning = 0.68,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Quailberry, Christmasberry",
frequency = 712.43,
latin = "Crossopetalum ilicifolium",
ordinal = 10,
panning = -0.64,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida boxwood",
frequency = 737.33,
latin = "Schaefferia frutescens",
ordinal = 11,
panning = 0.60,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Corkystem passionflower",
frequency = 809.26,
latin = "Passiflora suberosa",
ordinal = 12,
panning = -0.56,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Verdolaga-Francesa, Philippine-spinach, Ceylon-spinach",
frequency = 911.67,
latin = "Talinum fruticosum",
ordinal = 13,
panning = 0.52,
quadrat = 66,
wave = "triangle"
},
{
amplitude = 0.10,
common = "West Indian pinkroot",
frequency = 1082.33,
latin = "Spigelia anthelmia",
ordinal = 14,
panning = -0.48,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Stokes’ aster",
frequency = 1137.77,
latin = "Stokesia laevis",
ordinal = 15,
panning = 0.44,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pinebarren goldenrod",
frequency = 1196.50,
latin = "Solidago fistulosa",
ordinal = 16,
panning = -0.40,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Spanish-needles",
frequency = 1307.82,
latin = "Bidens alba var. radiata",
ordinal = 17,
panning = 0.36,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Common marigold, Aztec marigold, French marigold",
frequency = 1394.51,
latin = "Tagetes erecta",
ordinal = 18,
panning = -0.32,
quadrat = 66,
wave = "triangle"
},
{
amplitude = 0.10,
common = "Smooth strongback, Bahama strongbark",
frequency = 1673.34,
latin = "Bourreria succulenta",
ordinal = 19,
panning = 0.28,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Mouse’s pineapple, Redgal, Yellowroot, Cheese shrub",
frequency = 2041.78,
latin = "Morinda royoc",
ordinal = 20,
panning = -0.24,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Shiny-leaved wild coffee",
frequency = 2097.21,
latin = "Psychotria nervosa",
ordinal = 21,
panning = 0.20,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Man-in-the-ground, 'Bejuco colorado'",
frequency = 2399.71,
latin = "Ipomoea microdactyla",
ordinal = 22,
panning = -0.16,
quadrat = 66,
wave = "sine"
},
{
amplitude = 0.10,
common = "Holy basil, Tulsi",
frequency = 2943.83,
latin = "Ocimum tenuiflorum",
ordinal = 23,
panning = 0.12,
quadrat = 66,
wave = "square"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 24,
panning = -0.08,
quadrat = 66,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Lattice-vein fern",
frequency = 220.30,
latin = "Thelypteris reticulata",
ordinal = 1,
panning = -1,
quadrat = 71,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida arrowroot, coontie",
frequency = 225.57,
latin = "Zamia integrifolia",
ordinal = 2,
panning = 0.88,
quadrat = 71,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bluejacket, Ohio spiderwort",
frequency = 280.81,
latin = "Tradescantia ohiensis",
ordinal = 3,
panning = -0.76,
quadrat = 71,
wave = "sine"
},
{
amplitude = 0.10,
common = "Spanish-moss",
frequency = 292.65,
latin = "Tillandsia usneoides",
ordinal = 4,
panning = 0.64,
quadrat = 71,
wave = "sine"
},
{
amplitude = 0.10,
common = "Woodsgrass, Basketgrass",
frequency = 319.64,
latin = "Oplismenus hirtellus subsp. setarius",
ordinal = 5,
panning = -0.52,
quadrat = 71,
wave = "sine"
},
{
amplitude = 0.10,
common = "Twinberry, Simpson’s stopper",
frequency = 370,
latin = "Myrcianthes fragrans",
ordinal = 6,
panning = 0.40,
quadrat = 71,
wave = "sine"
},
{
amplitude = 0.10,
common = "Nakedwood, Soldierwood",
frequency = 583.69,
latin = "Colubrina elliptica",
ordinal = 7,
panning = -0.30,
quadrat = 71,
wave = "sine"
},
{
amplitude = 0.10,
common = "Corkystem passionflower",
frequency = 809.26,
latin = "Passiflora suberosa",
ordinal = 8,
panning = 0.20,
quadrat = 71,
wave = "sine"
},
{
amplitude = 0.10,
common = "Hog-plum, Tallowwood",
frequency = 868.92,
latin = "Ximenia americana",
ordinal = 9,
panning = -0.10,
quadrat = 71,
wave = "sine"
},
{
amplitude = 0.10,
common = "Frostweed, White crownbeard",
frequency = 1468.51,
latin = "Verbesina virginica ",
ordinal = 10,
panning = 0,
quadrat = 71,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Pine fern, Maidenhair pineland fern",
frequency = 209.98,
latin = "Anemia adiantifolia",
ordinal = 1,
panning = 1,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Southern shield fern",
frequency = 221.33,
latin = "Thelypteris kunthii",
ordinal = 2,
panning = -0.92,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Broad halbard fern",
frequency = 223.42,
latin = "Tectaria heracleifolia",
ordinal = 3,
panning = 0.84,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Virginia iris, Blue flag iris",
frequency = 245.21,
latin = "Iris virginica",
ordinal = 4,
panning = -0.76,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Meadow garlic",
frequency = 264.87,
latin = "Allium canadense",
ordinal = 5,
panning = 0.68,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida silver palm",
frequency = 271.70,
latin = "Coccothrinax argentata",
ordinal = 6,
panning = -0.62,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Woodsgrass, Basketgrass",
frequency = 319.64,
latin = "Oplismenus hirtellus subsp. setarius",
ordinal = 7,
panning = 0.56,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "White stopper",
frequency = 376.45,
latin = "Eugenia axillaris",
ordinal = 8,
panning = -0.50,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bahama senna, Chapman's wild sensitive plant",
frequency = 479.63,
latin = "Senna mexicana var. chapmanii",
ordinal = 9,
panning = 0.44,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Artillery fern, Artillery plant, Rockweed",
frequency = 622.94,
latin = "Pilea microphylla",
ordinal = 10,
panning = -0.38,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Maidenbush, Bahama maidenbush",
frequency = 696.42,
latin = "Heterosavia bahamensis",
ordinal = 11,
panning = 0.32,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Coastal Plain willow, Carolina willow",
frequency = 828.57,
latin = "Salix caroliniana",
ordinal = 12,
panning = -0.26,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Nopal",
frequency = 933.99,
latin = "Opuntia ficus-indica",
ordinal = 13,
panning = 0.20,
quadrat = 92,
wave = "triangle"
},
{
amplitude = 0.10,
common = "Scarlet milkweed, Bloodflower",
frequency = 2304.33,
latin = "Asclepias curassavica",
ordinal = 14,
panning = -0.14,
quadrat = 92,
wave = "triangle"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 15,
panning = 0.08,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Forked bluecurls",
frequency = 3377.85,
latin = "Trichostema dichotomum",
ordinal = 16,
panning = -0.04,
quadrat = 92,
wave = "sine"
},
{
amplitude = 0.10,
common = "Havana skullcap",
frequency = 3472.44,
latin = "Scutellaria havanensis",
ordinal = 17,
panning = 0,
quadrat = 92,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Florida arrowroot, coontie",
frequency = 225.57,
latin = "Zamia integrifolia",
ordinal = 1,
panning = -1,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Sargent's palm, Sargent's cherry palm, Buccaneer palm ",
frequency = 268.24,
latin = "Pseudophoenix sargentii",
ordinal = 2,
panning = 0.92,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bluejacket, Ohio spiderwort",
frequency = 280.81,
latin = "Tradescantia ohiensis",
ordinal = 3,
panning = -0.88,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bladdermallow",
frequency = 439.45,
latin = "Herissantia crispa",
ordinal = 4,
panning = 0.82,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Wild cotton, Upland cotton",
frequency = 447.94,
latin = "Gossypium hirsutum",
ordinal = 5,
panning = -0.76,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Yellow necklacepod",
frequency = 504.31,
latin = "Sophora tomentosa var. truncata",
ordinal = 6,
panning = 0.70,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Artillery fern, Artillery plant, Rockweed",
frequency = 622.94,
latin = "Pilea microphylla",
ordinal = 7,
panning = -0.64,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Coco-plum",
frequency = 790.49,
latin = "Chrysobalanus icaco",
ordinal = 8,
panning = 0.58,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Corkystem passionflower",
frequency = 809.26,
latin = "Passiflora suberosa",
ordinal = 9,
panning = -0.52,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Rougeplant, Bloodberry",
frequency = 889.99,
latin = "Rivina humilis",
ordinal = 10,
panning = 0.46,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Blanketflower, Firewheel",
frequency = 1291.22,
latin = "Gaillardia pulchella",
ordinal = 11,
panning = -0.40,
quadrat = 97,
wave = "triangle"
},
{
amplitude = 0.10,
common = "Spanish-needles",
frequency = 1307.82,
latin = "Bidens alba var. radiata",
ordinal = 12,
panning = 0.34,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scorpionstail",
frequency = 1787.40,
latin = "Heliotropium angiospermum",
ordinal = 13,
panning = -0.28,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Skyblue clustervine",
frequency = 2533.49,
latin = "Jacquemontia pentanthos",
ordinal = 14,
panning = 0.22,
quadrat = 97,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 15,
panning = -0.16,
quadrat = 97,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "False-sisal",
frequency = 252.34,
latin = "Agave decipiens",
ordinal = 1,
panning = 1,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bluejacket, Ohio spiderwort",
frequency = 280.81,
latin = "Tradescantia ohiensis",
ordinal = 2,
panning = -0.94,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Variable witchgrass",
frequency = 310.12,
latin = "Dichanthelium commutatum",
ordinal = 3,
panning = 0.88,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Ironwood, Redberry stopper",
frequency = 373.20,
latin = "Eugenia confusa",
ordinal = 4,
panning = -0.82,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bladdermallow",
frequency = 439.45,
latin = "Herissantia crispa",
ordinal = 5,
panning = 0.76,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Black ironwood",
frequency = 565.30,
latin = "Krugiodendron ferreum",
ordinal = 6,
panning = -0.70,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Artillery fern, Artillery plant, Rockweed",
frequency = 622.94,
latin = "Pilea microphylla",
ordinal = 7,
panning = 0.64,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Corkystem passionflower",
frequency = 809.26,
latin = "Passiflora suberosa",
ordinal = 8,
panning = -0.60,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pricklypear",
frequency = 956.96,
latin = "Opuntia humifusa",
ordinal = 9,
panning = 0.54,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Unknown goldenrod 2",
frequency = 1242.83,
latin = "Solidago sp2",
ordinal = 10,
panning = -0.48,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Southern-fleabane, Oakleaf fleabane ",
frequency = 1258.73,
latin = "Erigeron quercifolius",
ordinal = 11,
panning = 0.42,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Smooth strongback, Bahama strongbark",
frequency = 1673.34,
latin = "Bourreria succulenta",
ordinal = 12,
panning = -0.36,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scorpionstail",
frequency = 1787.40,
latin = "Heliotropium angiospermum",
ordinal = 13,
panning = 0.30,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Devil’s-potato, Rubbervine",
frequency = 2243,
latin = "Echites umbellatus",
ordinal = 14,
panning = -0.24,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scarlet milkweed, Bloodflower",
frequency = 2304.33,
latin = "Asclepias curassavica",
ordinal = 15,
panning = 0.18,
quadrat = 114,
wave = "triangle"
},
{
amplitude = 0.10,
common = "Swamp milkweed",
frequency = 2335.67,
latin = "Asclepias incarnata",
ordinal = 16,
panning = -0.12,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 17,
panning = 0.06,
quadrat = 114,
wave = "sine"
},
{
amplitude = 0.10,
common = "Thickleaf wild petunia",
frequency = 4158.54,
latin = "Ruellia succulenta",
ordinal = 18,
panning = 0,
quadrat = 114,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Blue agave",
frequency = 255.34,
latin = "Agave tequilana",
ordinal = 1,
panning = -1,
quadrat = 118,
wave = "square"
},
{
amplitude = 0.10,
common = "Woodsgrass, Basketgrass",
frequency = 319.64,
latin = "Oplismenus hirtellus subsp. setarius",
ordinal = 2,
panning = 0.92,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Silver buttonwood",
frequency = 351.74,
latin = "Conocarpus erectus",
ordinal = 3,
panning = -0.84,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bladdermallow",
frequency = 439.45,
latin = "Herissantia crispa",
ordinal = 4,
panning = 0.80,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Artillery fern, Artillery plant, Rockweed",
frequency = 622.94,
latin = "Pilea microphylla",
ordinal = 5,
panning = -0.78,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Locustberry",
frequency = 754.54,
latin = "Byrsonima lucida",
ordinal = 6,
panning = 0.72,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Corkystem passionflower",
frequency = 809.26,
latin = "Passiflora suberosa",
ordinal = 7,
panning = -0.66,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Wild plumbago, Doctorbush, Florida plumbago, White plumbago, Devil’s plumbago",
frequency = 848.46,
latin = "Plumbago zeylanica",
ordinal = 8,
panning = 0.60,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Rougeplant, Bloodberry",
frequency = 889.99,
latin = "Rivina humilis",
ordinal = 9,
panning = -0.56,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scorpionstail",
frequency = 1787.40,
latin = "Heliotropium angiospermum",
ordinal = 10,
panning = 0.52,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pearlberry, Tearshrub",
frequency = 2154.27,
latin = "Vallesia antillana",
ordinal = 11,
panning = -0.48,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pearlberry, Tearshrub",
frequency = 2154.27,
latin = "Vallesia antillana",
ordinal = 12,
panning = 0.42,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Southern river sage, Creeping sage",
frequency = 3067.52,
latin = "Salvia misella",
ordinal = 13,
panning = -0.34,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 14,
panning = 0.26,
quadrat = 118,
wave = "sine"
},
{
amplitude = 0.10,
common = "Wild-sage, Buttonsage",
frequency = 3988.59,
latin = "Lantana involucrata",
ordinal = 15,
panning = -0.14,
quadrat = 118,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "False-sisal",
frequency = 252.34,
latin = "Agave decipiens",
ordinal = 1,
panning = 1,
quadrat = 127,
wave = "sine"
},
{
amplitude = 0.10,
common = "Twinberry, Simpson’s stopper",
frequency = 370,
latin = "Myrcianthes fragrans",
ordinal = 2,
panning = -0.90,
quadrat = 127,
wave = "sine"
},
{
amplitude = 0.10,
common = "Sweet acacia",
frequency = 494.22,
latin = "Vachellia farnesiana var. farnesiana",
ordinal = 3,
panning = 0.80,
quadrat = 127,
wave = "sine"
},
{
amplitude = 0.10,
common = "Corkystem passionflower",
frequency = 809.26,
latin = "Passiflora suberosa",
ordinal = 4,
panning = -0.70,
quadrat = 127,
wave = "sine"
},
{
amplitude = 0.10,
common = "Rougeplant, Bloodberry",
frequency = 889.99,
latin = "Rivina humilis",
ordinal = 5,
panning = 0.60,
quadrat = 127,
wave = "sine"
},
{
amplitude = 0.10,
common = "Semaphore pricklypear",
frequency = 945.39,
latin = "Consolea corallicola",
ordinal = 6,
panning = -0.50,
quadrat = 127,
wave = "sine"
},
{
amplitude = 0.10,
common = "Yellow jessamine, Carolina jessamine",
frequency = 1055.79,
latin = "Gelsemium sempervirens",
ordinal = 7,
panning = 0.40,
quadrat = 127,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida privet, Florida swampprivet",
frequency = 2675.22,
latin = "Forestiera segregata",
ordinal = 8,
panning = -0.30,
quadrat = 127,
wave = "sine"
},
{
amplitude = 0.10,
common = "Southern river sage, Creeping sage",
frequency = 3067.52,
latin = "Salvia misella",
ordinal = 9,
panning = 0.20,
quadrat = 127,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 10,
panning = -0.10,
quadrat = 127,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Elliott’s love grass",
frequency = 324.61,
latin = "Eragrostis elliottii",
ordinal = 1,
panning = -1,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Muhlygrass, Hairawn muhly",
frequency = 329.73,
latin = "Muhlenbergia capillaris",
ordinal = 2,
panning = 0.90,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Powderpuff, Sunshine mimosa",
frequency = 489.29,
latin = "Mimosa strigillosa",
ordinal = 3,
panning = -0.80,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Downy milkpea",
frequency = 536.37,
latin = "Galactia volubilis",
ordinal = 4,
panning = 0.70,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Leavenworth’s tickseed",
frequency = 1359.08,
latin = "Coreopsis leavenworthii ",
ordinal = 5,
panning = -0.60,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Orange geigertree, Largeleaf geigertree",
frequency = 1717.98,
latin = "Cordia sebestena",
ordinal = 6,
panning = 0.50,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scorpionstail",
frequency = 1787.40,
latin = "Heliotropium angiospermum",
ordinal = 7,
panning = -0.40,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Devil’s-potato, Rubbervine",
frequency = 2243,
latin = "Echites umbellatus",
ordinal = 8,
panning = 0.34,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Beach morningglory",
frequency = 2432.44,
latin = "Ipomoea imperati",
ordinal = 9,
panning = -0.28,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Railroad vine, Bayhops",
frequency = 2465.64,
latin = "Ipomoea pes-caprae subsp. brasiliensis",
ordinal = 10,
panning = 0.22,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Southern fogfruit",
frequency = 3879.31,
latin = "Phyla stoechadifolia",
ordinal = 11,
panning = -0.16,
quadrat = 130,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pineland lantana, Rockland shrubverbena",
frequency = 4044.42,
latin = "Lantana depressa var. depressa",
ordinal = 12,
panning = 0.10,
quadrat = 130,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "West Indian-lilac, Florida clover ash",
frequency = 389.91,
latin = "Tetrazygia bicolor",
ordinal = 1,
panning = -1,
quadrat = 135,
wave = "sine"
},
{
amplitude = 0.10,
common = "Bahama senna, Chapman's wild sensitive plant",
frequency = 479.63,
latin = "Senna mexicana var. chapmanii",
ordinal = 2,
panning = 0.72,
quadrat = 135,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tough Florida bully",
frequency = 1030,
latin = "Sideroxylon tenax",
ordinal = 3,
panning = -0.56,
quadrat = 135,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pineland heliotrope",
frequency = 1763.93,
latin = "Heliotropium polyphyllum",
ordinal = 4,
panning = 0.40,
quadrat = 135,
wave = "sine"
},
{
amplitude = 0.10,
common = "Walter’s groundcherry",
frequency = 2603.33,
latin = "Physalis walteri",
ordinal = 5,
panning = -0.24,
quadrat = 135,
wave = "sine"
},
{
amplitude = 0.10,
common = "Horsemint, Spotted beebalm",
frequency = 3285.96,
latin = "Monarda punctata",
ordinal = 6,
panning = 0.10,
quadrat = 135,
wave = "sine"
}
],
[
{
amplitude = 0.10,
common = "Common fingergrass, Pinewoods fingergrass",
frequency = 335,
latin = "Eustachys petraea",
ordinal = 1,
panning = -1,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Mango",
frequency = 396.94,
latin = "Mangifera indica",
ordinal = 2,
panning = 0.92,
quadrat = 139,
wave = "triangle"
},
{
amplitude = 0.10,
common = "Powderpuff, Sunshine mimosa",
frequency = 489.29,
latin = "Mimosa strigillosa",
ordinal = 3,
panning = -0.84,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Coffee colubrina, Greenheart",
frequency = 571.34,
latin = "Colubrina arborescens",
ordinal = 4,
panning = 0.76,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Nakedwood, Soldierwood",
frequency = 583.69,
latin = "Colubrina elliptica",
ordinal = 5,
panning = -0.68,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Artillery fern, Artillery plant, Rockweed",
frequency = 622.94,
latin = "Pilea microphylla",
ordinal = 6,
panning = 0.60,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Pineland croton, Grannybush",
frequency = 680.86,
latin = "Croton linearis",
ordinal = 7,
panning = -0.52,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Corkystem passionflower",
frequency = 809.26,
latin = "Passiflora suberosa",
ordinal = 8,
panning = 0.44,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Leavenworth’s tickseed",
frequency = 1359.08,
latin = "Coreopsis leavenworthii ",
ordinal = 9,
panning = -0.38,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Eastern Purple Coneflower, Purple Coneflower",
frequency = 1548.91,
latin = "Echinacea purpurea ",
ordinal = 10,
panning = 0.32,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scorpionstail",
frequency = 1787.40,
latin = "Heliotropium angiospermum",
ordinal = 11,
panning = -0.26,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Butterflyweed, Butterfly milkweed",
frequency = 2273.45,
latin = "Asclepias tuberosa",
ordinal = 12,
panning = 0.22,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 13,
panning = -0.18,
quadrat = 139,
wave = "sine"
},
{
amplitude = 0.10,
common = "Oregano",
frequency = 3153.01,
latin = "Origanum vulgare",
ordinal = 14,
panning = 0.16,
quadrat = 139,
wave = "square"
},
{
amplitude = 0.10,
common = "Marjoram, Sweet marjoram, Knotted marjoram ",
frequency = 3196.69,
latin = "Origanum majorana",
ordinal = 15,
panning = -0.14,
quadrat = 139,
wave = "square"
},
{
amplitude = 0.10,
common = "Sweet almond verbena, Sweet almond bush",
frequency = 3773.14,
latin = "Aloysia virgata",
ordinal = 16,
panning = 0.12,
quadrat = 139,
wave = "triangle"
}
],
[
{
amplitude = 0.10,
common = "Tillandsia sp",
frequency = 288.59,
latin = "Tillandsia sp",
ordinal = 1,
panning = 1,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Woodsgrass, Basketgrass",
frequency = 319.64,
latin = "Oplismenus hirtellus subsp. setarius",
ordinal = 2,
panning = -0.92,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Spiny black-olive",
frequency = 357.65,
latin = "Terminalia molinetii",
ordinal = 3,
panning = 0.84,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Jamaica caper-tree",
frequency = 470.24,
latin = "Quadrella cynophallophora",
ordinal = 4,
panning = -0.76,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Powderpuff, Sunshine mimosa",
frequency = 489.29,
latin = "Mimosa strigillosa",
ordinal = 5,
panning = 0.68,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Blodgett’s wild mercury, Blodgett’s silverbush",
frequency = 665.75,
latin = "Argythamnia blodgettii",
ordinal = 6,
panning = -0.62,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Purslane, Little hogweed",
frequency = 922.75,
latin = "Portulaca oleracea",
ordinal = 7,
panning = 0.56,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "West Indian pinkroot",
frequency = 1082.33,
latin = "Spigelia anthelmia",
ordinal = 8,
panning = -0.52,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Spanish-needles",
frequency = 1307.82,
latin = "Bidens alba var. radiata",
ordinal = 9,
panning = 0.48,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Florida green-eyes",
frequency = 1430.97,
latin = "Berlandiera subacaulis",
ordinal = 10,
panning = -0.44,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Beach ragweed, Coastal ragweed",
frequency = 1507.15,
latin = "Ambrosia hispida",
ordinal = 11,
panning = 0.40,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Dense gayfeather, Blazing star",
frequency = 1587.85,
latin = "Liatris spicata ",
ordinal = 12,
panning = -0.36,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Sea-lavender, Sea-rosemary",
frequency = 1629.98,
latin = "Tournefortia gnaphalodes",
ordinal = 13,
panning = 0.32,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scorpionstail",
frequency = 1787.40,
latin = "Heliotropium angiospermum",
ordinal = 14,
panning = -0.28,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Scarlet milkweed, Bloodflower",
frequency = 2304.33,
latin = "Asclepias curassavica",
ordinal = 15,
panning = 0.24,
quadrat = 152,
wave = "triangle"
},
{
amplitude = 0.10,
common = "Tropical sage, Scarlet sage, Blood sage",
frequency = 3109.96,
latin = "Salvia coccinea",
ordinal = 16,
panning = -0.20,
quadrat = 152,
wave = "sine"
},
{
amplitude = 0.10,
common = "Blue porterweed, Joee",
frequency = 3569.79,
latin = "Stachytarpheta jamaicensis",
ordinal = 17,
panning = 0.16,
quadrat = 152,
wave = "sine"
}
]
]
