let pkgs = import <nixpkgs> { };
in pkgs.mkShell {
  buildInputs = with pkgs; [
    perl
    perlPackages.JSON
    perlPackages.TextCSV
    perlPackages.CaptureTiny
    perlPackages.ImportInto
    perlPackages.DBI
    perlPackages.SafeIsa
    perlPackages.PadWalker
    perlPackages.Autobox
  ];
}
