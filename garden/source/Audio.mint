module AudioContext {
  fun new : AudioContext {
    `new AudioContext()`
  }

  fun destination (ctx : AudioContext) : AudioDestinationNode {
    `#{ctx}.destination` as AudioDestinationNode
  }
}

module PlantSynth {
  fun new (
    freq : Number,
    amp : Number,
    pan : Number,
    timbre : String,
    ctx : AudioContext
  ) : PlantSynth {
    `new PlantSynth(#{freq}, #{amp}, #{pan}, #{timbre}, {context: #{ctx}})` as PlantSynth
  }

  fun start (plantSynth : PlantSynth) : Promise(Never, Void) {
    `#{plantSynth}.start()`
  }

  fun stop (plantSynth : PlantSynth) : Promise(Never, Void) {
    `#{plantSynth}.stop()`
  }

  fun stopAll (nodes : Array(PlantSynth)) : Promise(Never, Void) {
    `stopAll(#{nodes})`
  }
}
