component PlantList {
  connect Plants exposing { plants, toggle, plantActive }

  style plants {
    scrollbar-width: none;
    overflow: scroll;

    padding: 8px 12px;
    list-style: none;
    flex-grow: 1;
    margin: 0px;

    a {
      text-decoration: none;
    }

    &::-webkit-scrollbar {
      display: none;
    }
  }

  style plant (plant : Plant) {
    if (plantActive(plant)) {
      background: rgba(61, 121, 99, 0.57);
      font-style: default;
    } else {
      background: inherit;
      font-size: intalic;
    }

    color: rgb(207, 203, 197);
    border-radius: 0.3rem;
    margin: 8px 0px;

    font-family: serif;
    line-height: 30px;
    font-weight: 400;
    font-size: 16px;
  }

  fun render : Html {
    <ul::plants>
      for (plant of plants) {
        <a
          onClick={() { toggle(plant.latinName) }}
          aria-pressed="false"
          role="button"
          href="#">

          <li::plant(plant)>
            <{
              if (plantActive(plant)) {
                plant.commonName
              } else {
                plant.latinName
              }
            }>
          </li>

        </a>
      }
    </ul>
  }
}
