record PlantRaw {
  frequency : Number using "Frequency",
  amplitude : Number using "Amplitude",
  panning : Number using "Panning",
  quadrat : Number using "Quadrat",
  ordinal : Number using "Ordinal",
  common : String using "Common",
  latin : String using "Latin",
  wave : String using "Wave"
}

record Plant {
  synth : Maybe(PlantSynth),
  commonName : String,
  latinName : String,
  frequency : Number,
  amplitude : Number,
  waveType : String,
  panning : Number
}

module Plant {
  fun from (raw : PlantRaw) : Plant {
    {
      frequency = raw.frequency,
      amplitude = raw.amplitude,
      synth = Maybe.nothing(),
      commonName = raw.common,
      latinName = raw.latin,
      panning = raw.panning,
      waveType = raw.wave
    }
  }
}

record Quadrat {
  plants : Array(Plant),
  id : Number
}

store Plants {
  state audioContext : AudioContext = AudioContext.new()
  state quadrats : Map(Number, Quadrat) = Map.empty()
  state active : Map(String, Plant) = Map.empty()
  state selected : Number = 1

  get ids : Array(Number) {
    quadrats
    |> Map.values()
    |> Array.map(.id)
  }

  get plants : Array(Plant) {
    quadrats
    |> Map.get(selected)
    |> Maybe.map(.plants)
    |> Maybe.withDefault([])
  }

  fun load : Promise(Never, Void) {
    try {
      data =
        `window.plantData`

      rawData =
        decode data as Array(PlantRaw)

      next
        {
          quadrats =
            rawData
            |> Array.reduce(
              Map.empty(),
              (result : Map(Number, Quadrat), plant : PlantRaw) : Map(Number, Quadrat) {
                case (Map.get(plant.quadrat, result)) {
                  Maybe::Just quadrat =>
                    Map.set(
                      plant.quadrat,
                      { quadrat | plants = Array.push(Plant.from(plant), quadrat.plants) },
                      result)

                  Maybe::Nothing =>
                    Map.set(
                      plant.quadrat,
                      {
                        plants = [Plant.from(plant)],
                        id = plant.quadrat
                      },
                      result)
                }
              })
        }
    } catch {
      next {  }
    }
  }

  fun select (id : Number) : Promise(Never, Void) {
    sequence {
      next { selected = id }

      active
      |> Map.values()
      |> Array.map(
        (plant : Plant) : Promise(Never, Void) {
          case (plant.synth) {
            Maybe::Just synth => PlantSynth.stop(synth)
            Maybe::Nothing => next {  }
          }
        })

      next { active = Map.empty() }
    }
  }

  fun toggle (latinName : String) : Promise(Never, Void) {
    sequence {
      plant =
        Map.get(latinName, active)

      case (plant) {
        Maybe::Just plant =>
          sequence {
            case (plant.synth) {
              Maybe::Just synth => PlantSynth.stop(synth)
              Maybe::Nothing => next {  }
            }

            next { active = Map.delete(plant.latinName, active) }
          }

        Maybe::Nothing =>
          sequence {
            newPlant =
              plants
              |> Array.find(
                (p : Plant) : Bool {
                  p.latinName == latinName
                })
              |> Maybe.andThen(
                (plant : Plant) : Maybe(Plant) {
                  Maybe::Just(
                    { plant |
                      synth =
                        Maybe::Just(
                          PlantSynth.new(
                            plant.frequency,
                            plant.amplitude,
                            plant.panning,
                            plant.waveType,
                            audioContext))
                    })
                })

            case (newPlant) {
              Maybe::Just newPlant => next { active = Map.set(latinName, newPlant, active) }
              Maybe::Nothing => next {  }
            }
          }
      }
    }
  }

  fun plantActive (plant : Plant) : Bool {
    Map.has(plant.latinName, active)
  }
}
