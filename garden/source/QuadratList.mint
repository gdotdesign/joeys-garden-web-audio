component QuadratList {
  connect Plants exposing { select, selected, ids }

  style quadrats {
    border: 1px solid rgb(62, 68, 70);
    background: rgb(32, 35, 36);
    margin-left: 8px;
    margin-top: 8px;
    padding: 0px;
    width: 451px;

    scrollbar-width: none;
    overflow: scroll;

    color: rgb(232, 230, 227);
    font-weight: 400;
    font-size: 12px;

    &::-webkit-scrollbar {
      display: none;
    }
  }

  style quadrat (id : Number) {
    padding: 11px 16px;
    list-style: none;

    if (id == selected) {
      background: rgb(53, 57, 59);
    } else {
      background: inherit;
    }
  }

  fun render : Html {
    <ul::quadrats>
      for (id of ids) {
        <li::quadrat(id) onClick={() { select(id) }}>
          <{ "Quadrat #{id}" }>
        </li>
      }
    </ul>
  }
}
