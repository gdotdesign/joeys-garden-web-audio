component Main {
  style app {
    justify-content: left;
    flex-direction: row;
    align-items: left;

    /* align-content: stretch; */
    display: flex;

    background-color: rgb(24, 26, 27);
    height: 100vh;
    width: 100vw;

    font-family: Open Sans;
    font-weight: bold;
  }

  fun render : Html {
    <div::app>
      <QuadratList/>
      <PlantList/>
    </div>
  }
}
